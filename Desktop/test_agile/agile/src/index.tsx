import ReactDOM from 'react-dom/client';


import './css/font.css';
import'./css/font-awesome.min.css';
import './css/switcher.css';

import './css/index.css';

import { MetalibraryGUI } from './libraryGUI';
import { Color3 } from './Color';
import { Maths } from './Maths';

const Sleep = (s) => {
  return new Promise(resolve => setTimeout(resolve, s*1e3));
}



window.onload = async ()=>
{
  window.scroll(0,0)
  let PageLoader = document.getElementById('PageLoader')

async function ReadJsonDataFromUrl(url:string)
{
    const array = JSON.parse(await fetch(url).then(r=>r.text()))
    return array
}


MetalibraryGUI.SetupTheme(0,'#b9c2d8','#7c7f82','#696e68')

let NSA = false
const MaxProfileLogo = 15
let Base = 0
function RandomAvatar():string
{
   let Rd;
   // let Base = Math.floor(Maths.random(0,Math.floor(MaxProfileLogo/2)+1))
   if(!NSA)
     Rd=Math.floor(Maths.random(Base,MaxProfileLogo+3));
    else
     Rd=Math.floor(Maths.random(Base,MaxProfileLogo));
   Base=((Base+1)%MaxProfileLogo)
   if(Rd <= MaxProfileLogo)
   {
     
      return `http://localhost:3000/LogoProfile/Profile${Rd}.jpg`
   }
    else if(!NSA)
    {
      NSA = true
  
      return `http://localhost:3000/LogoProfile/MagicNSA.png`
    }
   
}


async function BuildDataApi()
{
 const Users:object[] = ((await ReadJsonDataFromUrl("https://dummyjson.com/users")).users as object[])

 for(let x=0;x<Users.length;x++)
 {
  let CurrentUser:{[key: string]: any} = (Users[x] as {[key: string]: any});
  CurrentUser.Avatar =  RandomAvatar();
  MetalibraryGUI.CreatePanel(CurrentUser, (document.getElementById("PanelDataApi_Container") as HTMLElement))
  
 }

}

await BuildDataApi()

function PxToPrc(px,maxp):number
{
  return  (( maxp - px ) / px)*100;
}
function PrcToPx(prc,maxp):number
{
  return Math.round(( prc/100 ) * maxp) 
}
function GenRandomLogo()
{
let basex=0,basey=0
  let TotalHeight = document.getElementById('PanelDataApi_Container').offsetHeight-250;
  for(let x=0; x<TotalHeight;x++)
{
  const IsValid = Maths.random(0,45)
  if(IsValid > 42)
  {
   
    let x =  Math.floor(Maths.random(basex,100))%100
    let y =  Math.floor(Maths.random(basey,100))%100
    if(PrcToPx(y,TotalHeight) > (TotalHeight-150))
         y -= 11;
    let logor = document.createElement('img')
    logor.src="http://localhost:3000/LogoProfile/agilelogo.svg"
    logor.style.position='absolute'
    logor.style.top=y+'%'
    logor.style.left=x+'%'
    logor.className = "logoagile"
    basex =(basex+8)%95
    basey = (basey+10)%95
    document.body.appendChild(logor)
  }
  
}
}

GenRandomLogo()

async function ChangeTheme(Type:boolean)
{
  let crt:HTMLElement = document.getElementById('crt')
  if(Type)
  { 
    document.body.style.setProperty('--text-color','wheat')
    crt.style.opacity='0.1'
   MetalibraryGUI.SetupTheme(50,'#090c0c','#101f1f','#0f2749')
  }
  else
  {
    document.body.style.setProperty('--text-color','black')
    crt.style.opacity='0.4'
    MetalibraryGUI.SetupTheme(-50,'#b9c2d8','#7c7f82','#696e68')
  }
}
MetalibraryGUI.Switcher(107,20,"fa fa-moon-o","fa fa-sun-o",false,ChangeTheme)
await Sleep(Maths.random(2,5))
PageLoader.remove();
document.body.style.overflow='scroll'



}


