import internal from "stream";
import { Maths } from "./Maths";

export class Color3
{
    public static hextodec(str:string): number[]
    {
        return [parseInt(str.substring(1,3),16),parseInt(str.substring(3,5),16),parseInt(str.substring(5,7),16)]
    };
    public static RandomColor(): string
    {
        return `rgb(${Maths.random(0,254)},${Maths.random(0,254)},${Maths.random(0,254)})`
    }
    public static hextorgb(hex:string):string
    {
        if(hex.includes('rgb('))
        {
            return hex
        }
       return `rgb(${this.hextodec(hex).join(',')})`; 
        
    }
    
    public static brigness(str:string| number[],n:number=0):string
    {
        if(typeof(str)=='string')
        {
            if(!(str as string).includes('rgb('))
        {
           throw "color format must be rgb(number(0-255),number(0-255),number(0-255))"
        }
        const  obj_v:string[] = (str as string).substring(4,(str as string).length-1).split(',')
        
        return `rgb(${(obj_v.map(v=>parseInt(v)+n)).join(',')})`

        }else if(typeof(str)=='object')
        {
            return `rgb(${((str as number[]).map(v=>v+n)).join(',')})`
        }
    }
}