import React from "react";
import internal from "stream";
import { Color3 } from "./Color";

export  class MetalibraryGUI extends React.Component
{
    render() {
        return <p>Lib</p>
    }
    static SetupTheme(Nbrigness,DefaultTheme:string,DefaultForeGroundTheme:string,DefaultBackground:string)
    {
        const PanelDataApi = document.getElementsByClassName('PanelDataApi')
    if(PanelDataApi.length>0)
    {
      Object.values(PanelDataApi).forEach((v:HTMLElement)=>
        {
            let NeonColor = (v.style.getPropertyValue('--neon-color')).replace(' ','');
            v.style.setProperty('--neon-color',Color3.brigness(NeonColor, Nbrigness));

        })
    }
        document.body.style.setProperty('--default-theme',DefaultTheme);
        document.body.style.setProperty('--fg-color',DefaultForeGroundTheme);
        document.body.style.setProperty('--bg-color',DefaultBackground);
    }
    
    static Switcher(x,y,IconChecked:string,IconUnChecked:string,Checked=false,callback=null,parent:null|HTMLElement=null):void
    {
       const Sw = document.createElement('input')
       Sw.className= "switch"
       Sw.type="checkbox"
       Sw.style.top = y + 'px';
       Sw.style.right = x + 'px';
       
       const Icon = document.createElement('i')
       Icon.id="theme_icon";
       Icon.className=IconUnChecked;
       Icon.style.top = (y+15) + 'px';
       Icon.style.right = (x-37) + 'px';
       Icon.style.zIndex='100'

       if(Checked)
       {
          Sw.click()
          Icon.className=IconChecked
          Icon.style.right = (x-79) + 'px';
          
       }
      if(callback!=null)
          Sw.onclick = (_)=>
          {
            if(Sw.checked)
            {
            Icon.className=IconChecked
            Icon.style.right = (x-79) + 'px';
            }
          else
          {
            Icon.className=IconUnChecked
            Icon.style.right = (x-37) + 'px';
          }
            callback(Sw.checked);
          }

       const RealParent = (parent||document.body as HTMLElement)
       RealParent.appendChild(Icon)
       RealParent.appendChild(Sw)
      

       //(<input id="theme" class="switch" type="checkbox">);
    }
    static CreatePanel(Data:{[key: string]: any},parent:HTMLElement|null=null):void
    {
       if(parent==null)
           return;

           parent.innerHTML += `<div class="PanelDataApi" style="--neon-color:${Color3.RandomColor()}">
 <img name="profile" src="${Data.Avatar}" >
 <h2>${Data.firstName}</h2>
 <h2>${Data.lastName}</h2>
 <h2>${Data.maidenName}</h2>
 <small>${Data.username}</small>
 <small>${Data.email}</small>
 <br>
 <div>───────</div>
 <br>
 <h3>${Data.birthDate}</h3>
 <h3>${Data.gender}</h3>
 <h3>${Data.height}cm / ${Data.weight}kg</h3>
 <br>
 <div name="BottomInformation">
     <h5>${Data.address.address}</h5>
     <br>
     <h5>${Data.address.city}</h5>
     <br>
     <small>${Data.address.city} , ${Data.address.postalCode}</small>
 </div>
</div>`

    }

}